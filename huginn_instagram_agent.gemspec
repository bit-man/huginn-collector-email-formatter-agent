# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "huginn_collector_email_formatter_agent"
  spec.version       = '0.4.3'
  spec.authors       = ["Alessio Signorini", "Víctor A. Rodríguez"]
  spec.email         = ["alessio@signorini.us", "victor@bit-man.guru"]

  spec.summary       = "Huginn Agent that monitors public Instagram accounts"

#   ToDo fix homepage
  spec.homepage      = "https://gitlab.com/bit-man/huginn-instagram-agent.git"


  spec.files         = Dir['LICENSE.txt', 'lib/**/*']
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = Dir['spec/**/*.rb'].reject { |f| f[%r{^spec/huginn}] }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", ">= 12.3.3"

  spec.add_runtime_dependency "huginn_agent"
#   ToDo wanna be deleted dependency
  spec.add_runtime_dependency "httparty"

end
