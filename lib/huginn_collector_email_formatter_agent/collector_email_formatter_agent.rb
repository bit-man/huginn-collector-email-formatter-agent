require_relative "message_service"

module Agents
    class CollectorEmailFormatterAgent < Agent
        can_dry_run!

        description <<-MD
        Format messages from Master Collector for delivery to Morning digest
        MD

        def working?
            true
        end

        def receive(incoming_events)
            log("receive: begin")
            events = Array.new
            incoming_events.each do |event|
                log("receive: #{event}")
                events.push(event)
            end
            service = MessageService.new
            message = service.invoke(events)
            log("receive: message #{message}")
            create_event :payload => message
            log("receive: end")
        end

#       Testing endpoint
        def receive_web_request(params, method, format)
            secret = params.delete('secret')
            return ["Please use POST requests only", 401] unless method == "post"
            return ["Not Authorized", 401] unless secret == interpolated['secret']
            log("Testing: receive_web_request #{params}")
            receive( params[items] )

            ['Done!', 200, 'text/plain']
        end
    end
end
