class MessageService

    @@urlStatus = [
        "https://www.kindpng.com/picc/m/405-4059088_icon-transparent-background-green-check-mark-hd-png.png",
        "http://clipart-library.com/images/pc7Ka76qi.png",
        "http://www.newdesignfile.com/postpic/2015/12/red-cross-icon_96454.png"
    ]
    
    def invoke(events)
        message = "<h2>Status</h2>\n<ul>"
        events.each do |event|
            message += "\n\t" + create_message_from(event)
        end
    
        message += "\n</ul>"
    end
    
    private
    
    def create_message_from(event)
        imgUrl = get_img_url(event.status)
        img="<img src='#{imgUrl}' height=18 width=18>"
        return "<li>#{img} #{event.name} : #{event.description}</li>"
    end
    
    def get_img_url(status)
        return @@urlStatus[status]
    end
end