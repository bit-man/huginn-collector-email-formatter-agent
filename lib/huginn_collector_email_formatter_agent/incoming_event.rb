class IncomingEvent
    def initialize(name, status, description)
        @name = name
        @status = status
        @description = description
    end

    attr_reader :status
    attr_reader :name
    attr_reader :description
end
